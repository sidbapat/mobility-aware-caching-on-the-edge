# Mobility-Aware Caching on the Edge

The performance of task offloading at the edge servers can be further enhanced by enabling these edge servers to cache the results. This would enable faster results for same requests in the future by another client.

Based on some of the previous works studied, a mobility-aware caching
scheme has been developed on the basis of an utility function.
In this scheme, mobility is defined as the ability of any user to
access any task result from anywhere and anytime. The inputs
are text files and for the simulation, the task is to count the
number of words in the input file. The architectural layout of
the simulation is as follows.
* There are three distinct network areas, each having a stationary/Low mobility user device that does not leaveits respective area and is continuously online. It caches only the result it recently obtained.
* Each of the network areas is served by two edge servers. Each of these servers collaborate amongst themselves and with edge servers at other areas to enquire about cached results. Each edge server caches only three results at a time.
* A control server keeps a record of all the files encountered in the system.
* Any number of mobile devices can connect to any of the edge servers at any area at any time. The simulation analysis of this scheme includes the performance of the system without any caching, caching only on the edge servers, and caching in edge servers as well as mobile devices.

# General Procedure:
* Caching only in the edge servers:
    * Each stationary user device initiates a request in its respective network area, caches the results locally, and is always available to share the cached results with future
mobile devices.
    * A mobile device initiates a request from any of the
three areas. It first checks for the cached results in the
stationary user device in the respective area.
    * If the results are not available in the stationary device,
the request is then forwarded to one of the edge servers
servicing their respective area.
    * The requested edge server first looks for the cached results locally. Then it collaborates with other edge servers
of the same and different network areas and if the results
are cached in any of the edge servers anywhere, it is
returned to the mobile user and a copy is sent to the
control server for record.
    * If the results are not cached anywhere, the requested
edge server then computes the request itself, forwards
the results to the mobile device and sends a copy to
the control server. The edge server then caches the new
results for future reference
* Caching in edge servers and mobile devices:
    * The procedure is similar to the previous implementation,
except the mobile devices perform additional operations
to search among other available mobile devices for the
cached results and cache their own results if necessary for
immediate future reference. Each mobile device caches
only one result at a time.
    * This implementation has an additional component known
as Node Directory to keep a record of the available mobile
devices in the system.
    * All mobile devices do not cache the results at all times.
Throughout the life cycle of the system, the mobile
devices are chosen in random for caching. Each mobile
device has a 50% chance of being chosen for caching
each time it initiates a fresh request.
    * Just as in the previous implementation, if the results
are not cached in the respective stationary device, the
requesting mobile device obtains the IP addresses of other
available mobile devices within the area from the Node
Directory.
    * The mobile device then checks with each of the other
available mobile devices within the area for the cached
results.
    * If the results are not cached anywhere, the device forwards the request to the edge server in the same manner
as the previous implementation.
    * If the results are cached anywhere in the system, irrespective of whether it has been chosen for caching or
not, it does not cache the obtained results as that would
result in duplicate caching in the system. If the results
were computed by the edge server, the mobile device
then caches the results only if it has been selected for
caching prior to the request.
    * If the mobile device has been selected for caching, then
upon completion of the task, it registers itself as available
to the Node Directory. The particular mobile device IP
address is recorded by the Node Directory for future
reference for other mobile devices looking for cached
results.
    * The mobile device remains active and available for about
1 minute and 30 seconds. Post the time limit, it notifies
the Node Directory and leaves the system. The results are
no longer cached and the Node Directory then removes
its IP address from the record.
 
## Utility Function:
In all the above caching schemes, each time a request is
forwarded by the mobile device to the edge server in the
respective area. The edge server is selected based on the
minimum time in seconds to forward the request using the
expression below:


time = size/bandwidth


where size is the size of the input file in megabytes (MB) and
bandwidth is the current bandwidth available to the respective
server in megabytes per second (MBps).
In all the above caching schemes, whenever an edge server
reaches its storage capacity, some result object is removed to
make way for the latest result object. This decision is based
on a trade-off between the frequency of the respective input
file and the size of the input file. Whenever an edge server
encounters a file whose corresponding result is cached locally,
it updates the frequency of the file. Among the stored results,
Rochester Institute of Technology 8 | P a g eRIT Computer Science • Capstone Report • 20205
the one with the lowest utility value is removed to make way
for the new result. The utility function is as below:


Utility Value = w1 ∗ (F<sub>obj</sub>/TF) + w2 * ((TS - S<sub>obj</sub>)/TS)


where F<sub>obj</sub> is the frequency of the object under consideration,
TF is the total frequency of all the objects currently cached
in the edge server, S<sub>obj</sub> is the size of the object under consideration and TS is the total size of all the objects currently
cached in the edge server. w1 and w2 are the weight factors
associated with frequency and size respectively. Higher the
particular weight factor, higher the priority of the respective
parameter. The idea is to maximise frequency and minimise
size.


## Execution Instructions:

### NOTE:
The IP addresses were hardcoded in a particular pattern to represent the types of components in the network. Any pattern can be used as per your convenience.


For all the input text files, remove all “$” and “~” symbols from the text file and then 
add just one “$” at the end of the file.
You also must make sure all the appropriate servers, clients are ready and all the input files 
are available , otherwise missing file or unavailable errors would occur.

1.  Activate Control Server, Each of the Edge Servers, and Node Directory (in case of 
mobile clients with caching). 
2. Run all stationary users. For “Simulation” Just run it. For normal execution, specify the file 
pathname and file name as the two main function arguments. 
The Control Server and the appropriate edge server would be updated as expected. 
The operations are displayed in the System.out for reference.
3. Run the mobile clients. For “Simulation” Just run it. For normal, specify the file 
pathname and file name as the two main function arguments. The results are 
different depending on caching ability.
The appropriate results are displayed in the System.out depending on the location of the cached contents such as mobile clients (if enabled), servers, stationary user etc.
4. The results displayed on the System.out are different based on non-caching mobile devices, caching mobile devices, location of the cached contents, unavailability of the cached contents etc. The entire operation is displayed in the system.out of each of the appropriately involved components for reference.
5. You can use any number of respective components according to the system architecture of your choice. Modify the hardcoded values such as IP addresses, port etc as appropriate for seamless collaborations.


## References:

1.  S. Wang, X. Zhang, Y. Zhang, L. Wang, J. Yang, and W. Wang, “A
survey on mobile edge networks: Convergence of computing, caching
and communications,” IEEE Access, vol. 5, pp. 6757–6779, 2017.
2.  K. Zhang, S. Leng, Y. He, S. Maharjan, and Y. Zhang, “Cooperative
content caching in 5g networks with mobile edge computing,” IEEE
Wireless Communications, vol. 25, no. 3, pp. 80–87, 2018.
3. J. Xu, L. Chen, and P. Zhou, “Joint service caching and task offloading
for mobile edge computing in dense networks,” in IEEE INFOCOM 2018 - IEEE Conference on Computer Communications, 2018, pp. 207–215.
4. C. Yi and H. Cho, “Distributed horizontal collaboration caching in edge
computing,” in 2018 International Conference on Computational Science
and Computational Intelligence (CSCI), 2018, pp. 1454–1455.
5. M. Chen, Y. Hao, M. Qiu, J. Song, D. Wu, and I. Humar,
“Mobility-aware caching and computation offloading in 5g ultra-dense
cellular networks,” Sensors, vol. 16, no. 7, 2016.
6. R. Wang, X. Peng, J. Zhang, and K. B. Letaief, “Mobility-aware caching
for content-centric wireless networks: modeling and methodology,” IEEE
Communications Magazine, vol. 54, no. 8, pp. 77–83, 2016.
7. R. Wang, J. Zhang, S. H. Song, and K. B. Letaief, “Mobility-aware
caching in d2d networks,” IEEE Transactions on Wireless Communications, vol. 16, no. 8, pp. 5001–5015, 2017.
8. H. Shen, M. S. Joseph, M. Kumar, and S. K. Das, “Precinct: a scheme
for cooperative caching in mobile peer-to-peer systems,” in 19th IEEE
International Parallel and Distributed Processing Symposium, 2005, pp.
10 pp.–.
9. W. H. O. Lau, M. Kumar, and S. Venkatesh, “A cooperative cache
architecture in support of caching multimedia objects in manets,”
in Proceedings of the 5th ACM International Workshop on Wireless
Mobile Multimedia, ser. WOWMOM ’02. New York, NY, USA:
Association for Computing Machinery, 2002, p. 56–63.


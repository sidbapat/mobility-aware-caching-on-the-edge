/*
 * StationaryCient.java
 *
 */

import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

/**
 * This program demonstrates the working of a stationary client that always remains active to service other mobile clients
 * and never leaves the area.
 * Modify the classname, objectname and address for additional stationary clients
 *
 * Author: Siddharth Bapat
 */

class StationaryClient1 extends Thread{

    //communication variables
    static Socket socket;
    ServerSocket clisersoc;
    InputStream clinputstr;
    OutputStream cloutputstr;
    FileReader filereader;
    FileWriter filewriter;
    int port=1000;
    int area=1;

    //this address is unique to a stationary client
    //127.1.1.0 represents area 1 stationary client 1 and so on
    //modify this according to your setup

    static String address="127.1.1.0";
    String clientaddress="";

    //can only cache 1 result
    static int MAX_SIZE=1;

    //to store cached results
    static HashMap<String,Integer> files=new HashMap<>();
    static ArrayList<Integer> filesize=new ArrayList<>();
    static ArrayList<Integer> filefrequecy=new ArrayList<>();

    //to keep a record of available edge servers
    static HashMap<Integer, ArrayList<String>> servers=new HashMap<>();

    /**
     * Initialises the record of all servers for reference
     */

    public void initialise_servertable(){
        ArrayList<String> serversofarea=new ArrayList<>();
        serversofarea.add("127.1.0.1");
        serversofarea.add("127.1.0.2");
        servers.put(1,serversofarea);
        serversofarea=new ArrayList<>();
        serversofarea.add("127.2.0.1");
        serversofarea.add("127.2.0.2");
        servers.put(2,serversofarea);
        serversofarea=new ArrayList<>();
        serversofarea.add("127.3.0.1");
        serversofarea.add("127.3.0.2");
        servers.put(3,serversofarea);

        //System.out.println("Server IPs according to Area code: ");
        //System.out.println(servers);
    }

    /**
     * This function enables the stationary client to service other mobile clients looking for results cached locally
     */

    public void run(){
        System.out.println("Another device has connected");
        StringBuilder inputfile=new StringBuilder();

        try {
            clinputstr=socket.getInputStream();
            cloutputstr=socket.getOutputStream();

            int inp=0;

            while((inp=clinputstr.read())>0){
                if((char)inp=='$'){
                    break;
                }
                inputfile.append((char)inp);

            }
            //checks for cached results and informs the requester as appropriate
                System.out.println("Checking if Results are cached locally.......");
                String[] names=inputfile.toString().split("=");
                System.out.println(names[1]);
                if(files.containsKey(names[1])) {
                    System.out.println("Results were cached locally, sent to requesting device");
                    cloutputstr.write((files.get(names[1]) + "$").getBytes());
                }else{
                    System.out.println("Results are not cached locally, notified the requesting device");
                    cloutputstr.write(("No$").getBytes());
                }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * This function initiates the task requests
     * @param   request     the input file
     * @param   fsize       the size of the file (MB)
     * @param   name        the name of the input file
     * @throws IOException
     */

    public void sendfiles(File request,float fsize,String name) throws IOException {

        //sends request to the edge servers in the same manner as mobile clients
        ArrayList<String> ser_ops=servers.get(area);
        ArrayList<Float> mintime=new ArrayList<>();
        for(int i=0;i<ser_ops.size();i++){
            float bandwidth=(float)((new Random().nextInt(13)+12)*0.125);
            float time=fsize/bandwidth;
            mintime.add(time);
            System.out.println("Time to load at Server "+ser_ops.get(i)+": "+time);
        }

        float min=mintime.get(0);
        int pos=0;
        for(int i=1;i<mintime.size();i++){
            if(mintime.get(i)<min){
                min=mintime.get(i);
                pos=i;
            }
        }

        String chosen=ser_ops.get(pos);
        System.out.println("Sending to server "+ser_ops.get(pos));
            socket = new Socket(chosen, port);
            clinputstr = socket.getInputStream();
            cloutputstr = socket.getOutputStream();

            //send name first to check for cached results
            cloutputstr.write(("Fname=" + name + "$").getBytes());
            int readfile = 0;
            String indicator = "";
            while ((readfile = clinputstr.read()) > 0) {
                if ((char) readfile == '$') {
                    break;
                }
                indicator += (char) readfile;
            }

            //System.out.println(indicator);

            cloutputstr.close();
            clinputstr.close();
            socket.close();

            //not cached, so the file contents are sent to the appropriate edge server
            if (indicator.contains("No")) {
                socket = new Socket(chosen, port);
                filereader = new FileReader(request);
                cloutputstr = socket.getOutputStream();
                clinputstr = socket.getInputStream();

                cloutputstr.write((fsize+"~").getBytes());
                while ((readfile = filereader.read()) > 0) {
                    cloutputstr.write(readfile);
                }
                System.out.println("Sent File to Server");
                indicator = "";
                while ((readfile = clinputstr.read()) > 0) {
                    if ((char) readfile == '$') {
                        break;
                    }
                    indicator += (char) readfile;
                }

                System.out.println("There are " + indicator + " words");

                cloutputstr.close();
                clinputstr.close();
                socket.close();

                /*caching-comment to disable*/
                if(files.size()>=MAX_SIZE){
                    files=new HashMap<>();
                }
                files.put(name, Integer.parseInt(indicator));
                /**/

            } else {
                System.out.println("Result is cached at the server");

                System.out.println("Result:");
                System.out.println("There are " + indicator + " words");

                /*caching-comment to disable*/
                if(files.size()>=MAX_SIZE){
                    files=new HashMap<>();
                }
                files.put(name, Integer.parseInt(indicator));
                /**/
            }

            clinputstr.close();
            cloutputstr.close();
            socket.close();

            //remains active to service other mobile clients
        clisersoc=new ServerSocket(port,50, InetAddress.getByName(address));
        while(true){

            socket=clisersoc.accept();
            new StationaryClient1().start();
        }

    }

    /**
     * The standard main function
     * @param args  the input file
     * @throws IOException
     */

    public static void main(String[] args) throws IOException {
        StationaryClient1 obj=new StationaryClient1();
        obj.initialise_servertable();
        System.out.println("Current IP address: "+address);


        //the user is expected to have provided the pathname to the input file as args[0] and filename (without extension)
        //as args[1]

        /*File file=new File(args[0]);
        float size=((float)file.length()/(float)1024)/(float)1024;
        System.out.println("File Size: "+size+" MB");
        obj.sendfiles(file,size,args[1]);*/


        //Same as above except all the file pathnames and names are hardcoded and selected randomly in order to speed up
        //the simulation. Based on your requirements you can comment/uncomment the simulation or the standard one
        /*For Simulation*/

        //input files
        //write the appropriate pathnames of the input files (Replace "Pathname"  with actual pathname)
        String[] fles={"C:"Pathname"\\input.txt","C:"Pathname"\\input2.txt",
                "C:"Pathname"\\input3.txt","C:"Pathname"\\input4.txt",
                "C:"Pathname"\\input5.txt","C:"Pathname"\\input6.txt",
                "C:"Pathname"\\input7.txt","C:"Pathname"\\input8.txt",
                "C:"Pathname"\\input9.txt","C:"Pathname"\\input10.txt",
                "C:"Pathname"\\input11.txt","C:"Pathname"\\input12.txt",
                "C:"Pathname"\\input13.txt","C:"Pathname"\\input14.txt",
                "C:"Pathname"\\input15.txt","C:"Pathname"\\input16.txt",
                "C:"Pathname"\\input17.txt","C:"Pathname"\\input18.txt",
                "C:"Pathname"\\input19.txt","C:"Pathname"\\input20.txt"};


        //input file names
        String[] nmes={"input","input2","input3","input4","input5","input6","input7","input8","input9","input10",
                "input11","input12","input13","input14","input15","input16","input17","input18","input19","input20"};


        int option=new Random().nextInt(20);

        File file=new File(fles[option]);

        float size=((float)file.length()/(float)1024)/(float)1024;
        System.out.println("File Size: "+size+" MB");
        obj.sendfiles(file,size,nmes[option]);
        /**/


    }
}
